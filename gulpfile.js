// ==========================================
// TABLE OF CONTENT
// ==========================================
// 1. Dependencies
// 2. Config
// 3. Functions
// 4. Gulp tasks


// ==========================================
// 1. DEPENDENCIES
// ==========================================
// gulp-dev-dependencies
const gulp = require('gulp');
// check package.json for gulp plugins
const gulpLoadPlugins = require('gulp-load-plugins');

// dev-dependencies
const browserSync = require('browser-sync').create();
const del = require('del');
const fs = require('fs');
const rollup = require('rollup').rollup;
const rollupBabel = require('rollup-plugin-babel');
const rollupNodeResolve = require('rollup-plugin-node-resolve');
const rollupUglify = require('rollup-plugin-uglify');
const runSequence = require('run-sequence');

const postcssAutoprefixer = require('autoprefixer');
const postcssCssnano = require('cssnano');

const $ = gulpLoadPlugins();


// ==========================================
// 2. CONFIG
// ==========================================
const config = {
  // BUILD TASKS
  tasks: ['pug', 'css', 'js', 'images', 'fonts'],
  // COMMAND ARGUMENTS
  cmd: {
    // check if "gulp --production"
    // http://stackoverflow.com/questions/28538918/pass-parameter-to-gulp-task#answer-32937333
    production: process.argv.indexOf('--production') > -1 || false,
  },
  // FOLDERS
  src: {
    folder: 'src/',
    data: {
      folder: 'src/data/',
      files: 'src/data/**/*.json',
    },
    fonts: 'src/fonts/**/*.*',
    img: 'src/img/**/*.{jpg,png,svg,gif}',
    js: {
      folder: 'src/js/',
      files: 'src/js/**/*.js',
      main: 'src/js/main.js',
      library: 'src/js/lib/',
      vendor: [],
      vendorFiles: 'src/js/vendor/**/*.js',
    },
    pug: {
      files: 'src/pug/**/*.pug',
      pages: 'src/pug/*.pug',
      partials: 'src/pug/_partials/**/*.pug',
    },
    scss: 'src/scss/**/*.scss',
  },
  tmp: {
    folder: 'tmp/',
    data: {
      folder: 'tmp/data/',
      file: 'tmp/data/data.json',
    },
  },
  dist: {
    folder: 'dist/',
    css: 'dist/assets/css/',
    fonts: 'dist/assets/fonts/',
    img: 'dist/assets/img/',
    js: 'dist/assets/js/',
    jsVendor: 'dist/assets/js/vendor/',
  },
  // plugin settings
  // AUTORPEFIXER
  autoprefixer: {
    cascade: true,
    precision: 10,
  },
  // SERVER
  browserSync: {
    // proxy: 'localhost:8888/display/web/',
    server: 'dist/',
    files: '**/*.html',
    ghostMode: {
      click: true,
      // location: true,
      forms: true,
      scroll: true,
    },
    injectChanges: true,
    logFileChanges: true,
    logLevel: 'info',
    notify: false,
    reloadDelay: 100,
    // startPath: "/cviceni/"
  },
  // POSTCSS
  postcss: {
    plugins: [
      postcssAutoprefixer(),
      postcssCssnano(),
    ],
  },
  // PUG
  pug: {
    // basedir: 'src/views/',
    pretty: true,
  },
  // ROLLUP
  rollup: {
    main: {
      bundle: {
        input: 'src/js/main.js',
        plugins: [
          rollupNodeResolve(),
          rollupBabel(),
          rollupUglify(),
        ],
      },
      output: {
        file: 'dist/assets/js/main.build.js',
        format: 'iife',
      },
    },
  },
  // SASS
  sass: {
    errLogToConsole: true,
    outputStyle: 'expanded',
  },
};


// ==========================================
// 3. FUNCTIONS
// ==========================================
function startBrowserSync() {
  if (browserSync.active) {
    return;
  }
  browserSync.init(config.browserSync);
}

function reload() {
  return browserSync.reload();
}


// ==========================================
// 4. TASKS
// ==========================================
// CLEAN
gulp.task('clean', done =>
  del([config.dist.folder, config.tmp.folder], done));


// DATA
gulp.task('data:merge', () =>
  gulp.src(config.src.data.files)
    .pipe($.plumber(config.plumber))
    // .pipe($.debug({
    //   title: 'data-source:',
    // }))
    .pipe($.jsoncombine('data.json', (data) => new Buffer(JSON.stringify(data))))
    // .pipe($.debug({ title: 'data-result:' }))
    .pipe(gulp.dest(config.tmp.data.folder)));
gulp.task('data:merge-watch', ['data:merge'], reload);


// PUG
gulp.task('pug', ['data:merge'], () =>
  gulp.src(config.src.pug.files)
    .pipe($.plumber(config.plumber))
    .pipe($.data(() => JSON.parse(fs.readFileSync(config.tmp.data.file))))
    .pipe($.pug(config.pug))
    .pipe(gulp.dest(config.dist.folder)));
gulp.task('pug-watch', ['pug'], reload);


// SASS
gulp.task('css', () =>
  gulp.src(config.src.scss)
    .pipe($.if(!config.cmd.production, $.sourcemaps.init()))
    .pipe($.sass(config.sass).on('error', $.sass.logError))
    .pipe($.if(config.cmd.production, $.postcss(config.postcss.plugins)))
    .pipe($.if(!config.cmd.production, $.sourcemaps.write('./maps')))
    .pipe(gulp.dest(config.dist.css))
    .pipe(browserSync.stream({ match: '**/*.css' })));


// JAVASCRIPT
// JS:VENDOR
gulp.task('js:vendor', () => {
  gulp.src(config.src.js.vendorFiles)
    .pipe(gulp.dest(config.dist.js));
});
gulp.task('js:vendor-watch', ['js:vendor'], reload);

// main
gulp.task('js:main', () =>
  rollup(config.rollup.main.bundle)
    .then((bundle) => {
      bundle.write(config.rollup.main.output);
    }));
gulp.task('js:main-watch', ['js:main'], reload);

gulp.task('js', ['js:vendor', 'js:main']);
gulp.task('js-watch', ['js'], reload);


// IMAGES
gulp.task('images', () =>
  gulp.src(config.src.img)
    .pipe($.if(config.cmd.production, $.imagemin(config.images)))
    .pipe(gulp.dest(config.dist.img)));
gulp.task('images-watch', ['images'], reload);


// FONTS
gulp.task('fonts', () =>
  gulp.src(config.src.fonts)
    .pipe(gulp.dest(config.dist.fonts)));
gulp.task('fonts-watch', ['fonts'], reload);


// SERVER
gulp.task('serve', () =>
  startBrowserSync());

// WATCH
gulp.task('watch', ['serve'], () => {
  // data
  gulp.watch(config.src.data.files, ['pug-watch']);
  // pug
  gulp.watch(config.src.pug.files, ['pug-watch']);
  // css
  gulp.watch(config.src.scss, ['css']);
  // js:app
  gulp.watch([config.src.js.files, `!${config.src.js.vendorFiles}`], ['js-watch']);
  // js:vendor
  gulp.watch(config.src.js.vendorFiles, ['js:vendor-watch']);
  // images
  gulp.watch(config.src.img, ['images-watch']);
  // fonts
  gulp.watch(config.src.fonts, ['fonts-watch']);
});

// GULP
gulp.task('default', ['clean'], () => {

  runSequence(config.tasks, () => {
    gulp.start('watch');
  });

});
