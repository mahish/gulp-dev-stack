export function _toggleClass($element, className) {
  if ($element.classList.contains(className)) {
    $element.classList.remove(className);
  } else {
    $element.classList.add(className);
  }
}

export function _toggleState($element, one, two) {
  $element.setAttribute('data-state', $element.getAttribute('data-state') === one ? two : one);
}
